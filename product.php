<?php
/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

/* Triggers when wp if fully loaded. */
add_action( 'init', 'register_product_type' );

/* Triggers when wp is adding meta boxes to the admin pages. */
add_action( 'add_meta_boxes', 'add_product_meta_boxes' );

/* Triggers when posts are saved. */
add_action( 'save_post', 'save_product_meta' );

/**
 * Creates and registers the Product post_type.
 */
function register_product_type() {
	$category                  = get_category_taxonomy();
	$origin                    = get_origin_taxonomy();
	$post_type                 = Laconst::PRODUCT;
	$args['label']             = 'Products';
	$args['labels']            = [
		'singular_name'      => __( 'Product', 'lacandona' ),
		'menu_name'          => __( 'Products', 'lacandona' ),
		'name_admin_bar'     => __( 'Product', 'lacandona' ),
		'add_new'            => __( 'Add New', 'lacandona' ),
		'add_new_item'       => __( 'Add New Product', 'lacandona' ),
		'new_item'           => __( 'New Product', 'lacandona' ),
		'edit_item'          => __( 'Edit Product', 'lacandona' ),
		'view_item'          => __( 'View Product', 'lacandona' ),
		'all_items'          => __( 'All Products', 'lacandona' ),
		'search_items'       => __( 'Search Products', 'lacandona' ),
		'not_found'          => __( 'No products found.', 'lacandona' ),
		'not_found_in_trash' => __( 'No products found in trash.', 'lacandona' )
	];
	$args['description']       = __( 'Product post type created by the lacandona plugin', 'lacandona' );
	$args['public']            = true;
	$args['menu_icon']         = 'dashicons-products';
	$args['rewrite']           = [ 'slug' => 'product' ];
	$args['supports']          = [ 'title', 'editor', 'thumbnail', 'excerpt', 'custom_fields' ];
	$args['show_in_nav_menus'] = true;
	$args['has_archive']       = true;
	$args['taxonomies']        = [ $category, $origin ];
	register_post_type( $post_type, $args );
}

/**
 * Creates and registers the Category taxonomy.
 */
function get_category_taxonomy() {
	$taxonomy             = Laconst::CATEGORY;
	$object_type          = [ Laconst::PRODUCT ];
	$args['label']        = __( 'Product Category', 'lacandona' );
	$args['labels']       = [ 'singular_name' => __( 'product category', 'lacandona' ), 'menu_name' => __( 'Product Categories', 'lacandona' ) ];
	$args['description']  = __( 'Product Category taxonomy created by the lacandona plugin', 'lacandona' );
	$args['hierarchical'] = true;
	$args['public']       = true;
	register_taxonomy( $taxonomy, $object_type, $args );
}

/**
 * Creates and registers the Origin taxonomy.
 */
function get_origin_taxonomy() {
	$taxonomy            = Laconst::ORIGIN;
	$object_type         = [ Laconst::PRODUCT ];
	$args['label']       = __( 'Origin', 'lacandona' );
	$args['labels']      = [ 'singular_name' => __( 'origin', 'lacandona' ) ];
	$args['description'] = __( 'Product Origin taxonomy created by the lacandona plugin', 'lacandona' );
	$args['public']      = true;
	register_taxonomy( $taxonomy, $object_type, $args );
}

/**
 * Adds the Product's meta boxes.
 */
function add_product_meta_boxes() {
	add_meta_box( Laconst::PRICE, __( 'Price', 'lacandona' ), 'create_info_mb', Laconst::PRODUCT );
	add_meta_box( Laconst::LINKID, __( 'Producer\'s Link', 'lacandona' ), 'create_link_mb', Laconst::PRODUCT );
}

/**
 * Creates the Price meta box HTML.
 */
function create_info_mb( $post ) {
	$saved_price = get_post_meta( $post->ID, Laconst::PRICE, true );
	$price_html     = '<div class="meta_box_div"><fieldset><input name="%1$s" id="%1$s" type="number" min="0.00" step="0.01" value="%2$.2f"/>€<fieldset></div><p>%3$s</p>';
	printf( $price_html, Laconst::PRICE, $saved_price, __( 'Enter price.', 'lacandona' ) );

	$saved_weight = get_post_meta( $post->ID, Laconst::WEIGHT, true );
	$weight_html     = '<div class="meta_box_div"><fieldset><input name="%1$s" id="%1$s" type="text" value="%2$s"/><fieldset></div><p>%3$s</p>';
	printf( $weight_html, Laconst::WEIGHT, $saved_weight, __( 'Enter package info.', 'lacandona' ) );
}

/**
 * Creates the Producer's Link meta box HTML.
 */
function create_link_mb( $post ) {
	$post_meta = get_post_meta( $post->ID );
	$linkid    = isset( $post_meta[ Laconst::LINKID ][0] ) ? $post_meta[ Laconst::LINKID ][0] : '0';
	//TODO: Use select multiple if needed.
	$mb_html  = '<div class="meta_box_div"><fieldset><select name="%1$s" id="%1$s">%2$s</select></fieldset></div><p>Coose a producer.</p>';
	$opt_html = '<option value="%s" %s>%s</option>';
	$opts     = sprintf( $opt_html, '0', '', 'No Link' );
	foreach ( get_link_options() as $linkopt ) {
		$opts .= sprintf( $opt_html, $linkopt->link_id, $linkopt->link_id === $linkid ? 'selected' : '', $linkopt->link_name );
	}
	printf( $mb_html, Laconst::LINKID, $opts );
}

function get_link_options() {
	global $wpdb;

	return $wpdb->get_results( "SELECT link_id,link_name FROM {$wpdb->prefix}links", OBJECT );
}

/**
 * Saves the Product's meta.
 */
function save_product_meta( $post_id ) {
	save_product_price( $post_id );
	save_product_weight( $post_id );
	save_product_linkid( $post_id );
}

/**
 * Saves the Price meta box value.
 */
function save_product_price( $post_id ) {
	// No need for nonce here.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( ! current_user_can( 'edit_post', $post_id ) ) ) { //TODO: Maybe save on autosave too.
		return;
	} elseif ( isset( $_POST['post_type'] ) && Laconst::PRODUCT === $_POST['post_type'] && isset( $_POST[ Laconst::PRICE ] ) ) {
		$float_price = floatval( $_POST[ Laconst::PRICE ] );
		update_post_meta( $post_id, Laconst::PRICE, $float_price > 0 ? $float_price : 0 );
	}
}

/**
 * Saves the Weight meta box value.
 */
function save_product_weight( $post_id ) {
	// No need for nonce here.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( ! current_user_can( 'edit_post', $post_id ) ) ) { //TODO: Maybe save on autosave too.
		return;
	} elseif ( isset( $_POST['post_type'] ) && Laconst::PRODUCT === $_POST['post_type'] && isset( $_POST[ Laconst::WEIGHT ] ) ) {
		$weight = strval( $_POST[ Laconst::WEIGHT ] );
		update_post_meta( $post_id, Laconst::WEIGHT, $weight );
	}
}

/**
 * Saves the Name and Link meta box values.
 */
function save_product_linkid( $post_id ) {
	// No need for nonce here.
	if ( ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) || ( ! current_user_can( 'edit_post', $post_id ) ) ) { //TODO: Maybe save on autosave too.
		return;
	} elseif ( isset( $_POST['post_type'] ) && Laconst::PRODUCT === $_POST['post_type'] ) {
		if ( isset( $_POST[ Laconst::LINKID ] ) && is_numeric( $_POST[ Laconst::LINKID ] ) ) {
			$linkid = intval( $_POST[ Laconst::LINKID ] );
			update_post_meta( $post_id, Laconst::LINKID, $linkid );

		}
	}
}
