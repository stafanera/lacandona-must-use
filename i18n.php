<?php

add_action( 'plugins_loaded', 'lacandona_textdomain' );

function lacandona_textdomain() {
	load_muplugin_textdomain( 'lacandona', basename( dirname( __FILE__ ) ) . DIRECTORY_SEPARATOR . 'languages' );
}