<?php
/** vim: fileencoding=utf-8

 ********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

/**
 * This class just adds a layer of uniqueness to our constants.
 */
abstract class Laconst{

    const CSSDIR   =  'css' . DIRECTORY_SEPARATOR;  // path css
    const JSDIR    =  'js'  . DIRECTORY_SEPARATOR;  // path js

    const VERSION  = '0.a.1';                       // version [js,css]

    const NAVMENU  = 'LacandoNav';                  // navigation
    const SOCMENU  = 'SocialMenu';                  // social links

    const PRODUCT  = 'lcdn_product';                // post_type
    const PRICE    = 'lcdn_price';                  // metabox
    const WEIGHT   = 'lcdn_weight';                 // metabox
    const LINKID   = 'lcdn_pname';                  // metabox
    const CATEGORY = 'lcdn_category';               // taxonomy
    const ORIGIN   = 'lcdn_origin';                 // taxonomy

    const EVENT    = 'lcdn_event';                  // post_type
    const DAY      = 'lcdn_date';                   // taxonomy

    const TAKEAWAY = 'TakeAway';                    // widget
    const SHOWCASE = 'Showcase';                    // widget

    const ADMINPG  = 'Lacandona';                   // menu page
    const ADMINSLG = 'lacandona_conf';              // page slug

    const WDADCSS  = 'lcdn_widgets_admin_css';      // css
    const WDGTCSS  = 'lcdn_widgets_css';            // css
    const WDADJS   = 'lcdn_widgets_admin_js';       // js
}
