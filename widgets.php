<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2018 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

/** Triggers after the default WordPress widgets have been registered. */
add_action('widgets_init', 'add_widgets');

/** Register the widget. */
function add_widgets(){
    register_widget(Laconst::TAKEAWAY);
    register_widget(Laconst::SHOWCASE);
}

/** Provided to enqueue extra scripts and styles.*/
add_action('wp_enqueue_scripts', 'lcdn_widgets_scripts');
/**
 * Registers and enqueues, styles and scripts for our widgets.
 */
function lcdn_widgets_scripts() {
    wp_register_style(Laconst::WDGTCSS, plugins_url(Laconst::CSSDIR . 'widgets.css', __FILE__), array(), Laconst::VERSION );
    wp_enqueue_style(Laconst::WDGTCSS);
}

/** Triggers first from the admin scripts actions. */
add_action('admin_enqueue_scripts', 'lcdn_widgets_admin_css');

/**
 * Registers and enqueues, styles and scripts for the widget menu on the dashboard.
 */
function lcdn_widgets_admin_css($suffix){
    if($suffix != 'widgets.php'){
        return;
    }
    wp_register_style(Laconst::WDADCSS, plugins_url(Laconst::CSSDIR . 'widgets_admin.css', __FILE__ ), array(), Laconst::VERSION);
    wp_enqueue_style(Laconst::WDADCSS);

    wp_register_script(Laconst::WDADJS, plugins_url(Laconst::JSDIR . 'widgets_admin.js', __FILE__), array('jquery'), Laconst::VERSION, /**in footer */false);
    wp_enqueue_script(Laconst::WDADJS);
}

/** Widget to display a menu with 'on the go' products and their prices. */
class TakeAway extends WP_Widget {

    /** The constructor. */
    function __construct() {
        $desc = 'Menu Widget for "take away" products.';
        parent::__construct(Laconst::TAKEAWAY, __('Take Away', 'lacandona'), array('description' => __($desc, 'lacandona')));
    }

    /** Outputs the content of the widget
     *
     * @param array $args       The theme arguments.
     * @param array $instance   The data.
     */
    public function widget($args, $instance){
        $title = __('Take Away', 'lacandona');;
        if(array_key_exists('title', $instance)){
            $title = apply_filters('widget_title', $instance['title']);
        }

        // before and after widget arguments are defined by themes
        echo($args['before_widget']);
        if(!empty($title)){
            printf("{$args['before_title']}$title{$args['after_title']}");
        }
        $i = 0;
        echo('<ul class="tkw_list">');
        while(array_key_exists("product_$i", $instance) && !empty($instance["product_$i"])){
            printf('<li><span class="tkw_product">%s</span><span class="tkw_price">%.02f</span></li>', $instance["product_$i"], $instance["price_$i"]);
            $i++;
        }
        echo('</ul>');
        echo($args['after_widget']);
    }

    /** Outputs the options form on admin.
     *
     * @param array $instance   The data.
     */
    public function form($instance){
        if(isset($instance['title'])){
            $title = esc_attr($instance['title']);
        }else{
            $title = __( 'Take Away', 'lacandona');
        }
        $title_id = $this->get_field_id('title');
        $title_name = $this->get_field_name('title');
        $admin_form = '<p><label for="%1$s">%3$s</label><input class="widefat" id="%1$s" name="%2$s" type="text" value="%4$s" /></p>';
        printf($admin_form, $title_id, $title_name, _e('Title:'), esc_attr($title));

        $field_num = 5;

        echo('<fieldset class="tkw_admin_bank">');
        printf("<legent>%s</legent><br/>", __('Product', 'lacandona'));
        for($i = 0; $i < $field_num; $i++){
            $product_name  = $this->get_field_name("product_$i");
            $product_id = $this->get_field_name("product_$i");
            $product_value = isset($instance["product_$i"]) && !empty( $instance["product_$i"])
                ? esc_attr($instance["product_$i"]) : null;
            $input_class = $product_value ? '' : 'tkw_hidden';
            if($i+1 == $field_num && empty($input_class)){
                $field_num+=$field_num;
            }

            echo("<input type=\"text\" name=\"$product_name\" id=\"$product_id\" class=\"tkw_product $input_class\" value=\"$product_value\"/>");
        }
        echo('</fieldset><!-- #tkw_admin_left -->');
        echo('<fieldset class="tkw_admin_bank">');
        printf("<legent>%s</legent><br/>", __('Price', 'lacandona'));
        for($i = 0; $i < $field_num; $i++){
            $price_name  = $this->get_field_name("price_$i");
            $price_id = $this->get_field_name("price_$i");
            $price_value = isset($instance["price_$i"]) ? esc_attr($instance["price_$i"]) : null;
            $input_class = isset($instance["product_$i"]) && !empty( $instance["product_$i"])
                ? '' : 'tkw_hidden';

            echo("<input type=\"number\" min=\"0.00\" step=\"0.1\" name=\"$price_name\" id=\"$price_id\" class=\"tkw_product $input_class\" value=\"$price_value\"/>");
        }
        echo('</fieldset><!-- #tkw_admin_right -->');
        echo('<div class="tkw_admin_clearfix"></div>');

        $add_product_value = __('Add Product');
        echo("<br/><input value=\"$add_product_value\" class=\"button-link\" type=\"button\" onclick=\"add_tkw_product();\"/>");
    }

    /** Update widget. */
    public function update($new_instance, $old_instance){
        $instance = $old_instance;
        $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
        $i = 0;
        while(isset($new_instance["product_$i"])){
            $instance["product_$i"] = !empty($new_instance["product_$i"]) ? strip_tags($new_instance["product_$i"]) : '';
            $instance["price_$i"] = !empty($new_instance["price_$i"]) ? strip_tags($new_instance["price_$i"]) : '';
            $i++;
        }
        return $instance;
    }
}

class Showcase extends WP_Widget {

    /** The constructor. */
    function __construct() {
        $desc = 'Showcases your products.';
        parent::__construct(Laconst::SHOWCASE, __('Showcase', 'lacandona'), array('description' => __($desc, 'lacandona')));
    }

    /** Outputs the content of the widget
     *
     * @param array $args       The theme arguments.
     * @param array $instance   The data.
     */
    public function widget($args, $instance){
        $title = __('Our Products', 'lacandona');;
        if(array_key_exists('title', $instance)){
            $title = apply_filters('widget_title', $instance['title']);
        }

        // before and after widget arguments are defined by themes
        echo($args['before_widget']);
        if(!empty($title)){
            printf("{$args['before_title']}$title{$args['after_title']}");
        }

        $query_args = array( 'post_type' => Laconst::PRODUCT, 'orderby'   => 'rand', 'posts_per_page' => /*TODO get option */ 4);
        $the_query = new WP_Query($query_args);
        if ( $the_query->have_posts() ) {
            echo('<ul class="showcase-list">');
            while ( $the_query->have_posts() ) {
                $the_query->the_post();
                printf('<li><a href="%s"><h5>%s</h5>', get_permalink(), get_the_title());
                if(has_post_thumbnail()){
                    the_post_thumbnail('thumbnail');
                }
                the_excerpt();
                echo('</a></li>');
            }
            echo('</ul>');
        }else{
            printf('<h5 class="notification">%s</h5>', __('No products found', 'lacandona'));
        }

        echo($args['after_widget']);
        /* Restore original Post Data */
        wp_reset_postdata();
    }

    /** Outputs the options form on admin.
     *
     * @param array $instance   The data.
     */
    public function form($instance){
        if(isset($instance['title'])){
            $title = esc_attr($instance['title']);
        }else{
            $title = __( 'Showcase', 'lacandona');
        }
        $title_id = $this->get_field_id('title');
        $title_name = $this->get_field_name('title');
        $admin_form = '<p><label for="%1$s">%3$s</label><input class="widefat" id="%1$s" name="%2$s" type="text" value="%4$s" /></p>';
        printf($admin_form, $title_id, $title_name, _e('Title:'), esc_attr($title));
        $quant_id = $this->get_field_id('quantity');
        $quant_name = $this->get_field_name('quantity');
        $quant_value = get_option('showcase_quantity', 4);
        printf('<input type="number" name="%s" id="%s" class="soc-product input_class" value="$display_num"/>', $quant_name, $quant_id, $quant_value);
        printf('<p>%s</p>', __('Number of Products to display', 'lacandona'));
    }

    /** Update widget. */
    public function update($new_instance, $old_instance){
        $instance = $old_instance;
        $instance['title'] = !empty($new_instance['title']) ? strip_tags($new_instance['title']) : '';
        if(isset($new_intance['quantity']) && is_int($new_instance['quantity'])){
            update_option('showcase_quantity', $new_instance['quantity']);
        }
        return $instance;
    }
}
