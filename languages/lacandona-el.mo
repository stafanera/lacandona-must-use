��    !      $  /   ,      �  =   �     '     /     ?  $   L  	   q     {     �     �     �     �     �     �     �               %     +     ;     C     V  9   g  7   �  1   �               $  	   -     7     D     c     j  �  {  N        e  #   v     �  H   �        )        =  !   W     y  &   �  P   �  '   	  7   -	     e	     x	     �	  %   �	     �	  '   �	  %   �	  c   #
  P   �
  N   �
     '  %   8     ^     x  !   �  .   �     �  %   �                         
                                        !                                                           	                                   Adapts Wordpress to Lacandona's post types and functionality. Add New Add New Product All Products All files were successfully updated. Bookmarks Edit Product Enter price. George Stamoulis New Product No products found No products found in trash. No products found. Number of Products to display Origin Our Products Price Producer's Link Product Product Categories Product Category Product Category taxonomy created by the lacandona plugin Product Origin taxonomy created by the lacandona plugin Product post type created by the lacandona plugin Products Search Products Showcase Take Away View Product What would you like to update? origin product category Project-Id-Version: Lacandona 1.1.190714
Report-Msgid-Bugs-To: https://wordpress.org/support/plugin/lacandona
Last-Translator: FULL NAME <EMAIL@ADDRESS>
Language-Team: LANGUAGE <LL@li.org>
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
POT-Creation-Date: 2019-07-14T21:24:22+00:00
PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE
X-Generator: WP-CLI 2.2.0
X-Domain: lacandona
 Προσαρμόζει το Wordpress στις ανάγκες της Lacandona Εισαγωγή Εισαγωγή Προϊόντος Όλα τα Προϊόντα Όλα τα αρχεία ενημερώθηκαν με επιτυχία. Σύνδεσμοι Επεξεργασία Προϊόντος Εισάγετε Τιμή Γιώργος Σταμούλης Νέο Προϊόν Δε βρέθηκαν Προϊόντα Δε βρέθηκαν προϊόντα στον κάδο ανακύκλωσης. Δε βρέθηκαν προϊόντα. Αριθμός προϊόντων για προβολή Προέλευση Τα Προϊόντα μας Κόστος Σύνδεσμος Παραγωγού Προϊόν Κατηγορίες Προϊόντων Κατηγορία Προϊόντος Εγγραφή Κατηγορίας Προϊόντος μέσω του πρόσθετου lacandona Εγγραφή Προέλευσης μέσω του πρόσθετου lacandona Εγγραφή Προϊόντος μέσω του πρόσθετου lacandona Προϊόντα Αναζήτηση Προϊόντος Τρία Προϊόντα Καφενείο Προβολή Προϊόντος Τι θέλετε να ενημερώσετε; προέλευση κατηγορία προϊόντος 