<?php
/**
 * @package Lacandona
 * vim: fileencoding=utf-8
 *
 * Plugin Name: Lacandona
 * Plugin URI: http://bitbucket.org/stafanera/lacandona-must-use
 * Description: Adapts Wordpress to Lacandona's post types and functionality.
 * Version: 1.1.200621
 * Author: George Stamoulis
 * Author URI: http://www.stamoulohta.com
 * License: LGPLv3
 * Text Domain: lacandona
 *
 ********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2019 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 *                                                                  *
 ********************************************************************
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

// Make sure we don't expose any info if called directly
if ( !function_exists( 'add_action' ) ) {
    echo 'Hi there!  I\'m just a plugin, not much I can do when called directly.';
    exit;
}

define('MAINDIR', dirname(__FILE__) . DIRECTORY_SEPARATOR);

include(MAINDIR . 'laconst.php');
include(MAINDIR . 'i18n.php');
include(MAINDIR . 'product.php');
include(MAINDIR . 'link.php');
include(MAINDIR . 'links_shortcode.php');
include(MAINDIR . 'widgets.php');
include(MAINDIR . 'exclusions.php');
include(MAINDIR . 'update.php');

if(WP_DEBUG) include(MAINDIR . 'debug.php');
