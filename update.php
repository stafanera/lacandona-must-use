<?php

add_action('admin_menu', 'lacandona_update_menu');

function lacandona_update_menu(){
    add_plugins_page(Laconst::ADMINPG, Laconst::ADMINPG, 'update_plugins', Laconst::ADMINSLG, 'lacandona_update_page');
}

/**
 * Prints the Update Page and updates the theme and the plugin
 * if it was requested by the user.
 */
function lacandona_update_page(){
    $plgin_repo = "https://bitbucket.org/stamoulohta/lacandona-must-use/get/HEAD.zip";
    $theme_repo = "https://bitbucket.org/stamoulohta/lacandona-theme/get/HEAD.zip";
    $stdout = Null;
    $new_index = basename(MAINDIR);
    if(isset($_POST['up_plgin'])){
        // TODO: Check if an update is needed.
        $zipfile = MAINDIR . DIRECTORY_SEPARATOR . 'lacandona-mup.zip';
        file_put_contents($zipfile, file_get_contents($plgin_repo));
        $stdout = unzip_over($zipfile, $new_index, dirname(MAINDIR) . DIRECTORY_SEPARATOR);
    }else if(isset($_POST['up_theme'])){
        $zipfile = get_theme_root() . DIRECTORY_SEPARATOR . 'lacandona' . DIRECTORY_SEPARATOR . 'lacandona-thm.zip';
        file_put_contents($zipfile, file_get_contents($theme_repo));
        $stdout = unzip_over($zipfile, $new_index, get_theme_root() . DIRECTORY_SEPARATOR);

    }

    $html = '<div class="wrap">
        <h1>%s</h1>
        %s
        <h2>%s</h2>
        <form id="template" name="template" method="post">
            <input type="submit" name="up_plgin" value="Update Plugin"/>
            <br/>
            <!--input type="submit" name="up_theme" value="Update Theme"/-->
        </form>
        </div><!-- .wrap -->';
    printf($html, Laconst::ADMINPG, $stdout, $stdout ? __('All files were successfully updated.', 'lacandona') : __('What would you like to update?', 'lacandona'));
}

/**
 * Renames all indices of the given zip archive
 * and extracts it under the new root directory.
 *      ** It overrides existing files. **
 *
 * @param $zfile    String  Path to the zip archive.
 * @param $destdir  String  The new root directory.
 *
 * @return          String  The names of files extracded.
 */
function unzip_over($zfile, $new_index, $destdir){
    $z = new ZipArchive();
    $stdout = '';
    if($z->open($zfile) === true){
        $old_index = basename($z->getNameIndex(0));
        if($old_index !== $new_index){
            for($i = 0; $i < $z->numFiles; $i++){
                $fname = $z->getNameIndex($i);
                $pos = strpos($fname, $old_index); // Returns the first occurence.
                if($pos !== false){
                    $new_fname = substr_replace($fname, $new_index, $pos, strlen($old_index));
                    $stdout .= $destdir . "$new_fname<br/>";
                    $z->renameIndex($i, $new_fname);
                }else{
                    $z->close();
                    exit(__("Not all files in same path!\n", 'lacanodna'));
                }
            }
        }
        $z->close(); // Archive needs to be closed and reopened
        if($z->open($zfile) === true){
            $z->extractTo($destdir);
            $z->close();
        }else{
            exit($z->getStatusString());
        }
    }
    return $stdout;
}
