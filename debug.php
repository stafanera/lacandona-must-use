<?php

/** Prints the variable given as parameter in a readable way and dies. */
function dd($var){
	echo('<pre>' . var_dump($var));
	die();
}