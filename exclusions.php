<?php
/** vim: fileencoding=utf-8

/********************************************************************
 *                                                                  *
 *    Copyright © George Stamoulis - 2017 - All Rights Reserved.    *
 *    This file is part of the Lacandona Wordpress plugin.          *
 *    Proprietary and confidential.                                 *
 *                                                                  *
 *    Unauthorized copying of any part of this file                 *
 *    via any medium is strictly prohibited.                        *
 ********************************************************************/

/**
 * Triggers when admin menu loads.
 */
add_action('admin_menu','remove_default_post_type');

/**
 * Triggers when admin menu loads.
 */
add_action('admin_menu','remove_add_media_button');

/**
 * Triggers when the 'At a Glance' widget loads.
 */
add_action( 'dashboard_glance_items', 'add_custom_posts_to_at_a_glance' );

/**
 * Removes default post types from the admin menu.
 * Note that users are still able to access the pages by url.
 */
function remove_default_post_type(){
    remove_menu_page('edit-comments.php');

    /*
        Dashboard          index.php
        Jetpack            jetpack
        Posts              edit.php
        Media              upload.php
        Pages              edit.php?post_type=page
        Comments           edit-comments.php
        Appearance         themes.php
        Plugins            plugins.php
        Users              users.php
        Tools              tools.php
        Settings           options-general.php
     */
}

/**
 * Removes the 'Add Media' button from the edmin menu.
 */
function remove_add_media_button(){
    remove_action('media_buttons', 'media_buttons');
}

/**
 * Prints html for custom post types.
 */
function add_custom_posts_to_at_a_glance(){
    $args = array('public' => true, '_builtin' => false);
    $post_types = get_post_types($args, 'object', 'and');
    foreach($post_types as $post_type){
        $num_posts = wp_count_posts($post_type->name);
        $num = number_format_i18n($num_posts->publish);
        $text = _n($post_type->labels->singular_name, $post_type->labels->name, intval($num_posts->publish));
        if(current_user_can('edit_posts')){
            // TODO Change the icons too.
            $output = '<a href="edit.php?post_type=' . $post_type->name . '">' . $num . ' ' . $text . '</a>';
            echo '<li class="post-count ' . $post_type->name . '-count">' . $output . '</li>';
        }
    }
}
