<?php

add_shortcode('wp_links', 'show_wp_links');

function show_wp_links() {
	$args = [
		'orderby'          => 'name',
	    'order'            => 'ASC',
	    'limit'            => -1,
//	    'category'         => ' ',
//	    'exclude_category' => ' ',
//	    'category_name'    => ' ',
//	    'hide_invisible'   => 1,
//	    'show_updated'     => 0,
	    'echo'             => 0,
	    'categorize'       => 1,
	    'title_li'         => __('Bookmarks', 'lacandona'),
	    'title_before'     => '<h2>',
	    'title_after'      => '</h2>',
	    'category_orderby' => 'name',
	    'category_order'   => 'ASC',
	    'class'            => 'linkcat',
	    'category_before'  => '<li id=%id class=%class>',
		'category_after'   => '</li>'
	];
	
	return wp_list_bookmarks($args);
}
