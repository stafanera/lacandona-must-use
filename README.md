# Lacandona Must Use WordPress Plugin

This plugin is meant to be used for adjusting the functionality of Wordpress to the needs of the lacandona web page. It is defined as a _must use_ plugin which means that it can't be deactivated via the dashboard.

## Features

* product post type
* link post type
* event post type (not fully implemented)
* showcase widget
* take away menu widget
* auto update

## Todo

* categories archive title
* facebook webhook
* quick edit for product price
* auto update notification

## Contact

* **developer**: Stamoulohta (g.a.stamoulis@gmail.com)
* **staff**:
    * Eleni
    * Nasia
    * Dimitra
    * Niovi

## License
This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation; either version 2 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program; if not, write to:

Free Software Foundation, Inc.
51 Franklin Street, Fifth Floor,
Boston, MA
02110-1301, USA.
